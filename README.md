# StackChat Common

This is a common NPM package for StackChat microservices.

## Registry setup

```
echo @stackchat:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

## Installation

```
npm i @stackchat/common
```