import {Injectable} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import {ExtractJwt} from 'passport-jwt';
import {Strategy} from 'passport-jwt';
import {Constants} from './constants';

export interface JwtPayload {
  sub: number;
  username: string;
}

export interface ContextUser {
  userId: number;
  username: string;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: Constants.secret,
    });
  }

  validate(payload: JwtPayload): ContextUser {
    return {userId: payload.sub, username: payload.username};
  }
}
